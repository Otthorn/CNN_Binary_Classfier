# CNN Binary Classifier

## Presenation
This project is a simple Convolutional Neural Network (CNN) used to classify images. It is a binary classifier which means that the goal is to be able
to distinguish between two types of images. In this example we are using memes: "Thinking emoji" and "Pepe the frog".

| ![example of a pepe](prepared_pepe/128_prepared.png) | ![example of thinking](prepared_thinking/58_prepared.png) |
|:--:|:--:|
| *Pepe the frog* | *Thinking emoji* |

This project uses the library Keras (with the Tensorflow backend) to build a really classical CNN model:

Conv2D $\to$ Relu $\to$ Pooling $\to$ Conv2D $\to$ Relu $\to$ Pooling $\to$ Conv2D $\to$ Relu $\to$ Pooling $\to$ Fully Connected

The number of hyperparameters being really low, they have been manully fine tuned.

This Network has been trained from the ground up (not using features extractions from bigger databases like ImageNet) and only using a consumer grade CPU. 

Training time on a `i7-7700K@4.2GHz`:
```
real	1m26.959s
user	7m54.009s
sys	0m44.387s
```

## Results

The model as a 92% accuracy on the validation dataset with the choosen hyperparamters. It uses only ~ 300 images of each. We could probably obtain better results by aquiring more data and tweaking hyperparameters a bit. This is a simple project aiming only to learn how to recreate state of the art algorithm from scracth to learn about them. Using an otheroptimizer might lead to better result.

![evolution of the model](acc_loss.png)

# The dataset
The dataset is already pre-processed, all images have been squared to 128x128 pixels for convinience. This pre-processing have been done using the squareup script [Fred's imagemagick scripts](http://www.fmwconcepts.com/imagemagick/squareup/index.php)

