import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np
import os


# ---------- IMPORTING AND PROCESSING THE DATA ---------------

database = []


def convert_color(arr):
    """Convert a grayscale array of shape (128,128) to a pseudo-RGB array with shape (128,128,3)"""
    if arr.shape == (128, 128):
        arr_rgb = np.zeros((128,128,3))
        arr_rgb[:,:,0] = arr
        arr_rgb[:,:,1] = arr
        arr_rgb[:,:,2] = arr
        arr = arr_rgb
    return arr

def img_to_database(folder):
    """Parse a folder into the database"""
    #os.chdir("./"+folder)

    for image in os.listdir(folder):
        img = mpimg.imread(folder + "/" + image)

        arr = np.array(img)

        arr = convert_color(arr)

        database.append(arr)


img_to_database("prepared_thinking")
N = len(database)

img_to_database("prepared_pepe")


x = np.array(database)
y = N*[1]+N*[0]
y = np.array(y)

x = x[:2*N]

def shuffle_in_unison(A, B):
    """Suffle 2 arrays in the same order"""
    assert len(A) == len(B)
    
    index_seed = np.arange(len(A))
    np.random.shuffle(index_seed)
    
    return A[index_seed], B[index_seed]

x, y = shuffle_in_unison(x, y)

def create_training_validation(x, y, p):
    """Create a training and validation dataset with a proportion p for the validation dataset."""
    n = len(x)
    x_train, x_val = x[:int(p*n)], x[int(p*n):]
    y_train, y_val = y[:int(p*n)], y[int(p*n):]
    
    return x_train, y_train, x_val, y_val

x_train, y_train, x_val, y_val = create_training_validation(x, y, 0.3)

#print(len(x), len(x_train), len(x_val))



# ----------------- CHECKING THE DATA -----------------------

def check_data(n):
    """enables manual check of data by printing the picture and the label"""
    if y[n] == 1:
        print("Thinking")
    elif y[n] == 0:
        print("Pepe")
    plt.imshow(x[n])
    plt.show()


# check_data(np.random.randint(0,len(x)))



# ------------------- BUILDING THE MODEL ----------------------

import keras
from keras.models import Sequential
from keras.layers import Dense, Flatten, Dropout, Conv2D, MaxPooling2D, Activation

BATCH_SIZE = 64
EPOCHS = 50

model = Sequential()

model.add(Conv2D(32, 3, input_shape=(128, 128, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Conv2D(32, 3))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(64, 3))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Flatten())
model.add(Dense(32))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(1))
model.add(Activation('sigmoid'))

model.compile(loss='binary_crossentropy',
                                optimizer='adadelta',
                                metrics =['accuracy'])



# ---------------- TRAINING AND EVALUATING THE MODEL -------------------

fit_history = model.fit(x_train, y_train,
        batch_size=BATCH_SIZE,
        epochs=EPOCHS,
        verbose=1)

loss_val, acc_val = model.evaluate(x_val, y_val)

print(f"On the validation set : loss = {loss_val*100:.2f} %, accuracy = {acc_val*100:.2f} %")

# print(fit_history.history.keys())

plt.subplot(211)
plt.plot(fit_history.history['acc'])
plt.xlabel("epochs")
plt.ylabel("accuracy")


plt.subplot(212)
plt.plot(fit_history.history['loss'])
plt.xlabel("epochs")
plt.ylabel("loss")

plt.savefig("acc_loss_2.png")

#plt.show()

